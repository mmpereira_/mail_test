'use strict';

describe('Controller: MainCtrl', function () {

    var scope, httpBackend, mainController;

  // load the controller's module
  beforeEach(module('mailTestApp'));


    beforeEach(
        inject(function($rootScope, $httpBackend, $controller) {
//            httpBackend = $httpBackend;

            scope = $rootScope.$new();
//
            mainController = function() {
                return $controller('MainCtrl', {
                    '$scope': scope
                });
            };
        }));

//    afterEach(function() {
//        httpBackend.verifyNoOutstandingExpectation();
//        httpBackend.verifyNoOutstandingRequest();
//    });


  // Initialize the controller and a mock scope
//  beforeEach(inject(function ($controller, $rootScope) {
//    scope = $rootScope.$new();
//    MainCtrl = $controller('MainCtrl', {
//      $scope: scope
//    });
//  }));

//  it('should attach a list of awesomeThings to the scope', function () {
//
//      var controller = mainController();
//
//
//      httpBackend.whenGET('data/folder.json').respond(200, [
//          {
//              "id": "1",
//              "name": "Inbox"
//          },
//          {
//              "id": "2",
//              "name": "Sent"
//          }
//      ]);
//
//      httpBackend.expect('GET','data/folder.json');
//
//
//
//      scope.$apply(function() {
//          scope.runTest();
//      });
//
//
//       expect(scope.folders.length).toEqual(2);
//
//
//  });

    it('should work', function() {
       var controller = mainController();

        scope.what();

        expect(scope.saywhat).toEqual(true);
    });
});
