/**
 * Created by mario on 12.10.14.
 */


'use strict';

describe('Folder', function() {

    var $httpBackend, mockFolderResource;

    beforeEach(angular.mock.module('mailTestApp'));

    beforeEach(
        inject(
            function($injector) {
                $httpBackend = $injector.get('$httpBackend');
                mockFolderResource = $injector.get('Folder');


                $httpBackend.whenGET('data/folder.json').respond(200, [{ id: "1", name: "spam"}]);

            }
        )
    );

    it('should return all folders', function() {

        $httpBackend.expectGET('data/folder.json');

        var result = mockFolderResource.query();

        $httpBackend.flush();

        expect(result[0].name).toEqual('spam');
    })
});

