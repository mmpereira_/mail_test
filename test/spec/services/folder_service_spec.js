/**
 * Created by mario on 12.10.14.
 */

'use strict';

describe('FolderService', function() {

    var $httpBackend, folderService, rootScope;
    var queryRequest, postRequest;

    beforeEach(angular.mock.module('mailTestApp'));

    beforeEach(
        inject(
            function($injector) {

                $httpBackend = $injector.get('$httpBackend');
                folderService = $injector.get('FolderService');
                rootScope = $injector.get('$rootScope');



                postRequest = $httpBackend.whenPOST('data/folder.json').respond(200, { id: "2", name: "my folder"});

            }
        )
    );

    it('it should retrieve all folders', function() {
        $httpBackend.whenGET('data/folder.json').respond(200, [{ id: "1", name: "spam"}]);
        $httpBackend.expectGET('data/folder.json');


        folderService.getFolders().then(function(data) {
            expect(data[0].name).toEqual("spaasdm");
        });

    });


//    it("should create a new folder", function() {
//        $httpBackend.expectPOST('data/folder.json');
//
//
//        folderService.createFolder('my folder').then(function(data) {
//            console.log('here!');
//            expect(data[0].name).toEqual("wha");
//        });
//
//        $httpBackend.flush();
//
//    });

    it("should return an error", function() {

        $httpBackend.whenGET('data/folder.json').respond(404);

        $httpBackend.expectGET('data/folder.json');


        folderService.getFolders().then(function(data) {
            expect(data.status).toEqual(404);
        });

        $httpBackend.flush();

    });

});
