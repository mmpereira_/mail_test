/**
 * Created by mario on 12.10.14.
 */

'use strict';

describe('Inbox', function() {

    var $httpBackend, mockInboxResource;

    beforeEach(angular.mock.module('mailTestApp'));

    beforeEach(
        inject(
        function($injector) {
            $httpBackend = $injector.get('$httpBackend');
            mockInboxResource = $injector.get('Mailbox');

                $httpBackend.whenGET('data/inbox.json').respond(200, [{ test: "hello"}]);
        })
    );

    it('it should retrieve all mails in the inbox', function() {


        $httpBackend.expectGET('data/inbox.json');

        var result = mockInboxResource.query();

        $httpBackend.flush();

        expect(result[0].test).toEqual("hello");


    })
});