/**
 * Created by mario on 15.10.14.
 */


angular.module('mailTestApp')
    .factory('ConfigService', function($resource, configParams) {

        return {

            urls : configParams.urls
        };

    });