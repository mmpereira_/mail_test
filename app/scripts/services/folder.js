/**
 * Created by mario on 12.10.14.
 */

'use strict';


angular.module('mailTestApp')
    .factory('Folder', function($resource, ConfigService, $httpBackend, $rootScope) {

        var folderResource = $resource(ConfigService.urls.crudFolderUrl, {}, {

            query: {
                method: "GET",
                isArray: true
            },

            get: {
                method: "GET"
            },

            delete: {
                method: "DELETE"
            }
        });

        var errorMessages = [
            {
                code: 404,
                message: 'Cant find anything'
            },
            {
                code: 400,
                message: 'Bad request'
            },
            {
                code: 'generic',
                message: "This is a generic error message"
            }
        ]




        function errorHandler(httpCode) {


            var message = _.findWhere(errorMessages, { code: httpCode });

            if(message == undefined)
                message = _.findWhere(errorMessages, { code: httpCode });

            return message;

        }



        return {

            query: function(params, success, failure) {

                folderResource.query(params,
                    function(data) {
                        success(data);
                    }, function(error) {
                        failure(errorHandler(error.status));
                    });
            },

            delete: function(params, success, failure) {


                $httpBackend.expect("DELETE", 'data/5.json').respond(200);
                $rootScope.$apply();

                folderResource.delete(params,
                    function(data) {
                        success(data);
                    }, function(error) {
                        failure(errorHandler(error.status));
                    });

                $httpBackend.flush();


            }
        }

    });