'use strict';

angular.module('mailTestApp')
  .factory('Mailbox', function ($resource) {


  	return $resource('data/inbox.json');

	});
