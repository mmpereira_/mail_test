/**
 * Created by mario on 12.10.14.
 */


'use strict';

angular.module('mailTestApp')
    .service('FolderService', function(Folder, $q) {

        var folders = [];


        this.createFolder = function(folderName) {

            var deferred = $q.defer();

            var found = _.findWhere(folders, { name: folderName });

            if(found == undefined) {
                // create folder

                var newFolder = new Folder({name : folderName});

                newFolder.$save().then(function(data) {
                    deferred.resolve(data);
                },
                function(error) {
                    deferred.resolve(error);
                });

            } else {
                // duplicate
                deferred.resolve(false);
            }

            return deferred.promise;

        };

        this.getFolders = function() {

            var deferred = $q.defer();

            if(folders.length > 0) {
                deferred.resolve(folders);
            } else {
                Folder.query({ id: "folder"},
                    function(data) {
                        folders = data;
                        deferred.resolve(folders);
                    }, function(error) {
                        deferred.reject(error);
                    });
            }



            return deferred.promise;

        };

        this.deleteFolder = function(folderId) {

            var deferred = $q.defer();

            Folder.delete({ id: folderId },
                function() {
                    folders = _.without(folders, _.findWhere(folders, {id: folderId}));

                    deferred.resolve();
                }, function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        this.saveFolder = function(folderId, newFolderName) {

            var deferred = $q.defer();

            deferred.reject({ msg: 'Duplicate name'});

            return deferred.promise;

        };


        this.moveFolders = function(cb) {


            var inbox = _.findWhere(folders, { name: 'Inbox' });
            cb(_.union(inbox, _.where(folders, { custom: true })));
        };

        this.updateUnreadMessage = function(fromFolderId, toFolderId) {

            console.log(fromFolderId, toFolderId);

            _.each(folders, function(f, index) {
               if(f.id == toFolderId) {
                   f.unreadCount ++;
                   folders[index] = f;
               } else if(f.id == fromFolderId) {
                   f.unreadCount --;
                   folders[index] = f;
               }
            });

        }
    });
