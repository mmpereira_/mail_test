'use strict';

angular.module('mailTestApp')
  .controller('MainCtrl', function ($scope, Mailbox, FolderService) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    Mailbox.query(function(data) {

    });




    $scope.deleteFolder = function(folderId) {

        FolderService.deleteFolder(folderId).then(
            function() {
                $scope.getFolders();
            },
            function(error) {
                $scope.errorMessage = error.message;
            }
        )
    }

    $scope.saveFolder = function(folder, cb) {



    }

    $scope.renameFolder = function() {
        $scope.showEditForm = true;
        $scope.style = "open";
    }

    $scope.cancelRename = function() {
        $scope.style = "";
        $scope.showEditForm = false;
    }


    $scope.getFolders = function() {


        FolderService.getFolders().then(
            function(data) {
                $scope.folders = [];
                $scope.customFolders = [];

                _.each(data, function(f) {
                    if(f.custom) {
                        $scope.customFolders.push(f);
                    } else $scope.folders.push(f);

                });
            }, function(reason) {
                console.log('REASON: ', reason);
            }
        );
    }

        $scope.getFolders();

  });
