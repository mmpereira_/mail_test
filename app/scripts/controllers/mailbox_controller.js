/**
 * Created by mario on 12.10.14.
 */

'use strict';

angular.module('mailTestApp')
    .controller('MailboxCtrl', function($scope, $state, $stateParams, FolderService, Mailbox, $translate) {

        $scope.showNotification = false;
        $scope.isError = false;

        FolderService.getFolders().then(function(folders) {


            $scope.folders = folders;

        });


        $scope.showRestoreButton = function() {
            return $stateParams.folder == 3 ? true : false;
        }


        $scope.$on('notification', function(event, args) {

            $translate(args.message, {folderName: args.param}).then(function(t) {
                $scope.notificationMessage = t;
                $scope.isError = args.isError;
                $scope.showNotification = true;
            });

        });

        $scope.move = function(folderId) {

            var selectedIds = ["1", "2"];

            _.each($scope.messages, function(m) {
                if(_.contains(selectedIds, m.id)) {

                    FolderService.updateUnreadMessage($stateParams.folder, folderId);
                }
            })

        }



        function retrieveMessages() {

//            $scope.moveFolders = FolderService.moveFolders();

            FolderService.moveFolders(function(folders) {
               $scope.moveFolders = folders;
            });

            Mailbox.query(function(data) {
                $scope.messages = data;
                });
        }

        retrieveMessages();
    });
