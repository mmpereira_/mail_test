/**
 * Created by mario on 26.10.14.
 */


'use strict';

angular.module('mailTestApp')
    .directive('createDropdown', function ($rootScope, FolderService) {

        return {
            restrict: 'A',
            templateUrl: "views/create_dropdown.html",
            link: function(scope, element, attrs) {


                scope.showErrorMessage = false;


                $(".dropdown-menu").bind('click', function (event) {
                    event.stopPropagation();
                });

                element.on('hide.bs.dropdown', function() {
                    console.log('closing ', scope);
                    scope.showErrorMessage = false;
                    scope.creationErrorMessage = "";
                });



                scope.create = function() {
                    console.log('create');

                    if(scope.newFolderName == 'asd') {
                        showError();
                    }

//                    closeDropdown();
                };

                scope.cancel = function() {
                    console.log('cancel');

                    closeDropdown();
                };


                function closeDropdown() {
                    $('[data-toggle="dropdown"]').parent().removeClass('open');
                    scope.showErrorMessage = false;
                };

                function showError() {
                  scope.showErrorMessage = true;
                    scope.creationErrorMessage = "Folder name already exists";
                };
            }
        }

    });