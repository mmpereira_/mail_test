/**
 * Created by mario on 14.10.14.
 */

'use strict';

angular.module('mailTestApp')
    .directive('myDropdown', function ($rootScope, FolderService) {
        return {
            restrict: 'A',
            templateUrl: "views/dropdown.html",
            link: function(scope, element, attrs) {


                $(".dropdown-menu").bind('click', function (event) {
                    event.stopPropagation();
                });


                element.on('hidden.bs.dropdown', function() {
                    scope.showRenameForm = false;
                    restartForm();
                });

                scope.delete = function(folder) {

                    FolderService.deleteFolder(folder.id).then(
                        function() {
                            $rootScope.$broadcast('notification', {
                                message: 'folder_delete_success',
                                param: folder.name,
                                isError: false
                            });
                            scope.$parent.getFolders();
                        },
                        function(error) {
                            scope.errorMessage = error.message;
                        }
                    )

                    closeDropdown();
                };

                scope.save = function(folder) {
                    FolderService.saveFolder(folder.id, scope.newFolderName).then(
                        function(sucess) {

                        }, function(error) {
                            scope.errorMessage = error.msg;

                        }
                    )
                };

                scope.cancel = function() {
                    closeDropdown();
                };

                scope.updateInput = function() {
                    scope.errorMessage = "";
                }



                function closeDropdown() {
                    restartForm();
                    $('[data-toggle="dropdown"]').parent().removeClass('open');
                }

                function restartForm() {
                    scope.showRenameForm = false;
                    scope.newFolderName = "";
                    scope.errorMessage = "";

                }


            }

        };
    });