'use strict';

angular
  .module('mailTestApp', [
    'ngResource',
    'ui.router',
    'ngMockE2E',
    'pascalprecht.translate'
  ])
    .constant("configParams", {
        urls: {
            crudFolderUrl: "http://localhost:3000/api/mail"
        }
    })
    .config(function($stateProvider, $urlRouterProvider, $translateProvider, $httpProvider) {
        //
        // For any unmatched url, redirect to /state1


        //
        // Now set up the states
        $stateProvider
            .state('state1', {
                url: "/state1/:folder",
                templateUrl: "views/state1.html",
                controller: 'MailboxCtrl'
            })
            .state('state2', {
                url: "/state2",
                templateUrl: "views/state2.html"
            })
            .state('search', {
                url: "/search/:terms",
                templateUrl: "views/state1.html",
                controller: "MailboxCtrl"
            });

        $urlRouterProvider.otherwise("/state1/1");


        $translateProvider.translations('en', {
            TITLE: 'Hello',
            FOO: 'This is a paragraph.',
            BUTTON_LANG_EN: 'english',
            BUTTON_LANG_DE: 'german',
            folder_delete_success : "Folder '{{folderName}}' has been deleted !"
        });

        $translateProvider.preferredLanguage('en');


//        $httpProvider.defaults.useXDomain = true;
//        delete $httpProvider.defaults.headers.common['X-Requested-With'];

    })
    .run(function ($location, $rootScope, $state, $stateParams, $httpBackend) {

        if($location.path() === '/error') {
            $location
                .path('/')
                .replace();
        }

        $httpBackend.whenGET(/.*/).passThrough();

    });;
